provider "gitlab" {
    token = var.gitlab_token
    version = "~> 2.5"
}