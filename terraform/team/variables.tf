variable "gitlab_token" {
  type = string
  description = "The user access token to manage GitLab"
}


variable "gitlab-group-id" {
  type = number
  description = "GitLab Group ID to manage"
}
