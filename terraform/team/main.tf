# data "gitlab_user" "nagyv" {
#   username = "nagyv-gitlab"
# }

# resource "gitlab_group_membership" "nagyv" {
#   group_id     = var.gitlab-group-id
#   user_id      = data.gitlab_user.nagyv.user_id
#   access_level = "owner"
# }

data "gitlab_user" "nicholas" {
  username = "nicholasklick"
}

resource "gitlab_group_membership" "nicholas" {
  group_id     = var.gitlab-group-id
  user_id      = data.gitlab_user.nicholas.user_id
  access_level = "owner"
}

data "gitlab_user" "magdalena" {
  username = "m_frankiewicz"
}

resource "gitlab_group_membership" "magdalena" {
  group_id     = var.gitlab-group-id
  user_id      = data.gitlab_user.magdalena.user_id
  access_level = "owner"
}

# data "gitlab_user" "alexives" {
#   username = "alexives"
# }

# resource "gitlab_group_membership" "alexives" {
#   group_id     = var.gitlab-group-id
#   user_id      = data.gitlab_user.alexives.user_id
#   access_level = "owner"
# }

data "gitlab_user" "mattkasa" {
  username = "mattkasa"
}

resource "gitlab_group_membership" "mattkasa" {
  group_id     = var.gitlab-group-id
  user_id      = data.gitlab_user.mattkasa.user_id
  access_level = "owner"
}

data "gitlab_user" "grzesiek" {
  username = "grzesiek"
}

resource "gitlab_group_membership" "grzesiek" {
  group_id     = var.gitlab-group-id
  user_id      = data.gitlab_user.grzesiek.user_id
  access_level = "owner"
}

data "gitlab_user" "emilyring" {
  username = "emilyring"
}

resource "gitlab_group_membership" "emilyring" {
  group_id     = var.gitlab-group-id
  user_id      = data.gitlab_user.emilyring.user_id
  access_level = "owner"
}

data "gitlab_user" "mikegreiling" {
  username = "mikegreiling"
}

resource "gitlab_group_membership" "mikegreiling" {
  group_id     = var.gitlab-group-id
  user_id      = data.gitlab_user.mikegreiling.user_id
  access_level = "owner"
}

data "gitlab_user" "mvrachni" {
  username = "mvrachni"
}

resource "gitlab_group_membership" "mvrachni" {
  group_id     = var.gitlab-group-id
  user_id      = data.gitlab_user.mvrachni.user_id
  access_level = "owner"
}

data "gitlab_user" "ali" {
  username = "ali-gitlab"
}

resource "gitlab_group_membership" "ali" {
  group_id     = var.gitlab-group-id
  user_id      = data.gitlab_user.ali.user_id
  access_level = "owner"
}

data "gitlab_user" "tauriedavis" {
  username = "tauriedavis"
}

resource "gitlab_group_membership" "tauriedavis" {
  group_id     = var.gitlab-group-id
  user_id      = data.gitlab_user.tauriedavis.user_id
  access_level = "owner"
}