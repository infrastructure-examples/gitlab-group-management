provider "gitlab" {
    token = var.gitlab_token
    version = "~> 2.5"
}

data "gitlab_group" "group" {
  group_id = 6985750
}

module "team" {
  source = "../team"

  gitlab-group-id = data.gitlab_group.group.group_id
  gitlab_token = var.gitlab_token
}
