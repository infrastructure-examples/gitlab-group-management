variable "gitlab_token" {
  type = string
  description = "The user access token to manage GitLab"
}
