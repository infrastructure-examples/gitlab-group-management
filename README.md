# GitLab team management example

This project shows how the [System team](https://about.gitlab.com/handbook/engineering/development/ops/configure/system/) manages access to its example projects.

## TODO

## Projects to manage

Every team member should get maintainer access to:
- https://gitlab.com/gitlab-org/configure
- https://gitlab.com/infrastructure-examples
- https://gitlab.com/knative-examples/

Technial:

- set up remote state
- import existing members into state
- set up CI/CD job to run for all our repos
